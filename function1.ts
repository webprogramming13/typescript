function getTime(): number {
    return new Date().getTime();
}
console.log(getTime());

function printHello(): void {
    console.log("Hello");
}
printHello();

function multiply(a: number, b: number): number {
    return a * b;
}
console.log(multiply(1, 2));

function add(a: number, b: number, c?: number) {
    return a + b + (c || 0);
}

console.log(add(1, 2, 3));
console.log(add(1, 2));


function pow(vale: number, exponent: number = 10): number {
    return vale ** exponent;
}

console.log(pow(10));
console.log(pow(10, 2));

function divide({ divided, divisor }: { divided: number, divisor: number }) {
    return divided / divisor;
}

console.log(divide({ divided: 100, divisor: 10 }));

function add2(a: number, b: number, ...rest: number[]) {
    return a + b + rest.reduce((p, c) => p + c, 0);
}
console.log(add2(1, 2, 3, 4, 5));

type Nagate = (vale: number) => number;

const nagateFunction: Nagate = (vale: number) => vale * -1;
const nagateFunction2: Nagate = function (value: number): number {
    return value * -1;
}
console.log(nagateFunction(1));
console.log(nagateFunction2(1));